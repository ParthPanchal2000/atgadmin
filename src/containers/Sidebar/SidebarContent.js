import React from "react";
import {Menu} from "antd";
import {Link} from "react-router-dom";
import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import UserProfile from "./UserProfile";

import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import {useSelector} from "react-redux";


const SidebarContent = () => {

  let {navStyle, themeType, pathname} = useSelector(({settings}) => settings);
  const getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };

  const selectedKeys = pathname.substr(1);
  const defaultOpenKeys = selectedKeys.split('/')[1];
  
  return (
    <>
      <SidebarLogo/>
      <div className="gx-sidebar-content">
        <div className={`gx-sidebar-notifications ${getNoHeaderClass(navStyle)}`}>
          <UserProfile/>
     
        </div>
        <CustomScrollbars className="gx-layout-sider-scrollbar">
         
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">
                 {/* <Menu.Item key="/Table/Task">
                  <Link to="/Table/Task" style={{color:'white'}}> <i className="icon icon-edit"/>
                    <span><IntlMessages id="Task"/></span></Link>
                </Menu.Item> */}

                <Menu.Item key="main/dashboard/crm">
                  <Link to="/main/dashboard/crm" style={{color:'white'}}> <i className="icon icon-home"/>
                    <span><IntlMessages id="Home"/></span></Link>
                </Menu.Item> 
        
                <Menu.Item key="/Table/Artcat">
                  <Link to="/Table/Artcat"  >
                    <i className="icon icon-anchor"/><span><IntlMessages
                      id="Art Category"/></span></Link>
                </Menu.Item>

                <Menu.Item key="/Table/Artsubcat">
                  <Link to="/Table/Artsubcat"  >
                    <i className="icon icon-crm"/><span><IntlMessages
                      id="Art Subcategory"/></span></Link>
                </Menu.Item>

                <Menu.Item key="Table/Bidding">
                  <Link to="/Table/Bidding"  >
                    <i className="icon icon-pricing-table"/><span><IntlMessages
                      id="Bidding"/></span></Link>
                </Menu.Item>

                <Menu.Item key="/Table/City">
                  <Link to="/Table/City"  >
                    <i className="icon icon-table"/><span><IntlMessages
                      id="City"/></span></Link>
                </Menu.Item>

                <Menu.Item key="Table/Contactus">
                  <Link to="/Table/Contactus"  >
                    <i className="icon icon-phone"/><span><IntlMessages
                      id="Contact Us"/></span></Link>
                </Menu.Item>

                <Menu.Item key="/Table/Expereincelevel">
                  <Link to="/Table/Expereincelevel"  >
                    <i className="icon icon-etherium"/><span><IntlMessages
                      id="Experience Level"/></span></Link>
                </Menu.Item>

                <Menu.Item key="/Table/Feedback">
                  <Link to="/Table/Feedback"  >
                    <i className="icon icon-feedback"/><span><IntlMessages
                      id="Feedback"/></span></Link>
                </Menu.Item>
                
                <Menu.Item key="/Table/Login">
                  <Link to="/Table/Login"  >
                    <i className="icon icon-signin"/><span><IntlMessages
                      id="Login"/></span></Link>
                </Menu.Item>

                {/* <Menu.Item key="/Table/Payment">
                  <Link to="/Table/Payment"  >
                    <i className="icon icon-table"/><span><IntlMessages
                      id="payment"/></span></Link>
                </Menu.Item> */}
               
                
                <Menu.Item key="/Table/Project">
                  <Link to="/Table/Project" >
                  <i className="icon icon-folder"/><span><IntlMessages
                      id="Project"/></span></Link>
                </Menu.Item>
                
                <Menu.Item key="/Table/Registration">
                  <Link to="/Table/Registration"  >
                    <i className="icon icon-signup"/><span><IntlMessages
                      id="Registration"/></span></Link>
                </Menu.Item>
                <Menu.Item key="/Table/Subartcat">
                  <Link to="/Table/Subartcat"  >
                    <i className="icon icon-treeselect"/><span><IntlMessages
                      id="Sub Art Category"/></span></Link>
                </Menu.Item>

                <Menu.Item key="/Table/Userartcat">
                  <Link to="/Table/Userartcat"  >
                    <i className="icon icon-ripple"/><span><IntlMessages
                      id="User Art Category"/></span></Link>
                </Menu.Item>
                <Menu.Item key="/Table/Usertype">
                  <Link to="/Table/Usertype"  >
                    <i className="icon icon-profile"/><span><IntlMessages
                      id="User Type"/></span></Link>
                </Menu.Item>
                {/* <Menu.Item key="social-apps/profile">
                  <Link to="/Components/User" >
                    <i className="icon icon-profile2"/>
                    <span><IntlMessages id="User"/></span>
                  </Link>
                </Menu.Item>
           */}
                {/* <Menu.Item key="components/table/basic">
                  <Link to="/ProjectList" >
                  <i className="icon icon-folder"/><span><IntlMessages
                      id="Project"/></span></Link>
                </Menu.Item> */}

                {/* <Menu.Item key="extra-components/editor/ck">
                  <Link to="/extra-components/editor/ck" >
                  <i className="icon icon-editor"/><span><IntlMessages
                      id="Invoice"/></span></Link>
                </Menu.Item>

                <Menu.Item key="Testimonial">
                  <Link to="/custom-views/extras/testimonials" >
                  <i className="icon icon-quote-forward"/><span><IntlMessages
                        id="Testimonial"/></span></Link>
                </Menu.Item> */}
                  
               

                {/* <MenuItemGroup key="Verify" className="gx-menu-group"
                              title={<IntlMessages id="Verify"/>}> */}
                  {/* <SubMenu key="verify" className={getNavStyleSubMenuClass(navStyle)}
                          title={<span><i className="icon icon-auth-screen"/><span><IntlMessages
                            id="Verify"/></span></span>}>
                  
                  </SubMenu> */}
                  {/* <Menu.Item key="verify">
                        <Link to="customViews/extras/testimonials" >
                        <i className="icon icon-auth-screen"/><span><IntlMessages
                            id="Verify"/></span></Link>
                      </Menu.Item> */}

                {/* </MenuItemGroup> */}

          </Menu>
        </CustomScrollbars>
      </div>
    </>
  );
};

SidebarContent.propTypes = {};
export default SidebarContent;

