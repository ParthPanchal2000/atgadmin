import React from "react";
// import {Avatar} from "antd";

// const userImageList = [
//   {
//     id: 1,
//     image: require("assets/images/image2.jpg"),
//   },
//   {
//     id: 2,
//     image: require("assets/images/image2.jpg"),
//   },
//   {
//     id: 3,
//     image: require("assets/images/image2.jpg"),

//   },
//   {
//     id: 4,
//     image: require("assets/images/image2.jpg"),
//     name: 'Mila Alba',
//     rating: '5.0',
//     deals: '27 Deals'
//   },
// ]

export const aboutList = [
  {
    id: 1,
    title: 'Works at',
    icon: 'company',
    userList: '',
    desc: ['Artisttoget']
  },
  {
    id: 2,
    title: 'Birthday',
    icon: 'birthday-new',
    userList: '',
    desc: ['Oct 25, 1984']
  },
 
  {
    id: 3,
    title: 'Lives in London',
    icon: 'home',
    userList: '',
    desc: ['From Switzerland']
  },
  
];

export const eventList = [
  {
    id: 1,
    image: require("assets/images/image2.jpg"),
    title: 'Sundance Film Festival.',
    address: 'Downsview Park, Toronto, Ontario',
    date: 'Feb 23, 2019',
  },
  {
    id: 2,
    image: require("assets/images/image2.jpg"),
    title: 'Underwater Musical Festival.',
    address: 'Street Sacramento, Toronto, Ontario',
    date: 'Feb 24, 2019',
  },
  {
    id: 3,
    image: require("assets/images/image2.jpg"),
    title: 'Village Feast Fac',
    address: 'Union Street Eureka',
    date: 'Oct 25, 2019',
  }
];

export const contactList = [
  {
    id: 1,
    title: 'Email',
    icon: 'email',
    desc: [<span className="gx-link" key={1}>kiley.brown@example.com</span>]
  },
  {
    id: 2,
    title: 'Web page',
    icon: 'link',
    desc: [<span className="gx-link" key={2}>example.com</span>]
  }, {
    id: 3,
    title: 'Phone',
    icon: 'phone',
    desc: ['+1-987 (454) 987']
  },
];

export const friendList = [
  {
    id: 1,
    image: require("assets/images/image2.jpg"),
    name: 'Chelsea Johns',
    status: 'online'

  },
  {
    id: 2,
    image: require("assets/images/image2.jpg"),
    name: 'Ken Ramirez',
    status: 'offline'
  },
  {
    id: 3,
    image: require("assets/images/image2.jpg"),
    name: 'Chelsea Johns',
    status: 'away'

  },
  {
    id: 4,
    image: require("assets/images/image2.jpg"),
    name: 'Ken Ramirez',
    status: 'away'
  },
];
