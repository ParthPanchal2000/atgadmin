import React from "react";
import IconWithTextCard from "components/dashboard/CRM/IconWithTextCard";
import {Col,Button,Divider,Card,Table,Modal} from "antd";
import {Link} from "react-router-dom";

const confirm = Modal.confirm;
function showConfirm() {
    confirm({
      title: 'Do you Want to delete these items?',
   
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: text => <span className="gx-link">{text}</span>,
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
         
              <Link to="../../update/ProfileUpdate"><span className="gx-link"><Button><i className="icon icon-custom-view"/></Button></span></Link>
            <Divider type="vertical"/>
        <span className="gx-link"><Button ><i className="icon icon-edit"/></Button></span>
        <Divider type="vertical"/>
        <span className="gx-link"><Button onClick={showConfirm}><i className="icon icon-trash"/></Button></span>
      </span>
      ),
    }
  ];
  
  const data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
    }
  ];
  
const User=()=>{
    return(
        <>
       
        <Col xl={6} lg={6} md={6} sm={12} xs={12}>
              <IconWithTextCard cardColor="orange" icon="wall" title="687" subTitle="Atrist"/>
              <IconWithTextCard cardColor="blue" icon="wall" title="687" subTitle="Hire"/>
            </Col>
        <Col>
        <Card title="User">
    
         <Table className="gx-table-responsive" columns={columns} dataSource={data}/>
     </Card>
        </Col>           
         
        </>
    );
};
export default User;