import React from "react";
import {Route, Switch} from "react-router-dom";

import Components from "./components/index";
import CustomViews from "./customViews/index";
import Extensions from "./extensions/index";
import ExtraComponents from "./extraComponents/index";

import SocialApps from "./socialApps/index";
import Main from "./main/index";
import ProjectList from "./ProjectList/ProjectList";
import Update from "./update";
import Table from "./Table";
const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}main`} component={Main}/>
      <Route path={`${match.url}components`} component={Components}/>
      <Route path={`${match.url}custom-views`} component={CustomViews}/>
      <Route path={`${match.url}extensions`} component={Extensions}/>
      <Route path={`${match.url}extra-components`} component={ExtraComponents}/>
      <Route path={`${match.url}social-apps`} component={SocialApps}/>
      <Route path={`${match.url}ProjectList`} component={ProjectList}/>
      <Route path={`${match.url}Update`} component={Update}/>
      <Route path={`${match.url}Table`} component={Table}/>
    </Switch>

  </div>
);

export default App;
// Main =Home
//inbuit-app=table
//  Social app = Profile