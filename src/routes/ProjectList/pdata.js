
export const cardsList = [
  {
    name: 'Domnic Harris',
    avatar: require("assets/images/project1.png"),
    description: "All the Lorem Ipsum generators on the Internet...",
    rate: "17",
    earning: "45",
    position: "Graphic Designer/ UI & UX"
  },
  {
    name: 'Garry Sobars',
    avatar: require("assets/images/project1.png"),
    description: "It uses a dictionary of over 200 Latin words, combined ...",
    rate: "20",
    earning: "88",
    position: "PHP Developer"
  },
  {
    name: 'Stella Johnson',
    avatar: require("assets/images/project1.png"),
    description: "The generated Lorem Ipsum is therefore always...",
    rate: "24",
    earning: "102",
    position: "Java Developer"
  }, {
    name: 'John Smith',
    avatar: require("assets/images/project1.png"),
    description: "It is a long established fact that a reader will...",
    rate: "18",
    earning: "67",
    position: "PHP Developer"
  },
  {
    name: 'Alex Dolgove',
    avatar: require("assets/images/project1.png"),
    description: "Many desktop publishing packages and web page editors... ",
    rate: "19",
    earning: "65",
    position: "Graphic Designer/ UI & UX"
  },
  {
    name: 'Domnic Brown',
    avatar: require("assets/images/project1.png"),
    description: "There are many variations of passages of Lorem Ipsum ...",
    rate: "13",
    earning: "43",
    position: "Graphic Designer/ UI & UX"
  },
];
