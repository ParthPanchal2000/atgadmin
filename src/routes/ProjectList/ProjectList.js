import React from "react";
import { cardsList } from "./pdata";
import {Col, Row,Button,Divider,Card,Table,Modal} from "antd";
import ContainerHeader from "components/ContainerHeader/index";
import CardsListItem from "../customViews/listType/Component/CardsListItem";
import IconWithTextCard from "components/dashboard/CRM/IconWithTextCard";

const confirm = Modal.confirm;
function showConfirm() {
    confirm({
      title: 'Do you Want to delete these items?',
   
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <span className="gx-link">{text}</span>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
            <span className="gx-link"><Button><i className="icon icon-custom-view"/></Button></span>
          <Divider type="vertical"/>
      <span className="gx-link"><Button><i className="icon icon-edit"/></Button></span>
      <Divider type="vertical"/>
      <span className="gx-link"><Button onClick={showConfirm}><i className="icon icon-trash"/></Button></span>
    </span>
    ),
  }
];

const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  }
];
const ProjectList =({match})=> {
  return (
    <div className="gx-main-content gx-pb-sm-4">
      <Row>
        <Col xl={6} lg={6} md={6} sm={12} xs={12}>
             <IconWithTextCard cardColor="blue" icon="folder" title="12" subTitle="Project"/>
        </Col>

        <Col span={24}>
          <Card title="User">
            <Table className="gx-table-responsive" columns={columns} dataSource={data}/>
          </Card>
        </Col> 

        <Col span={24}>
          <ContainerHeader title="ProjectList " match={match}/>
        </Col>
        
        <Col span={24}>  
          {cardsList.map((data, index) => (
            <CardsListItem key={index} data={data} styleName="gx-card-list"/>
          ))}
        </Col>
      </Row>
    </div>
  )
};

export default ProjectList;
