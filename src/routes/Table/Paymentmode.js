import React from "react";
import {Col,Row,Card,Button,Divider, Table} from "antd";
import IconWithTextCard from "../../components/dashboard/CRM/IconWithTextCard";



const columns = [{
    title: 'Name',
    dataIndex: 'name',
    render: text => <span className="gx-link">{text}</span>,
  }, {
    title: 'Age',
    dataIndex: 'age',
  }, {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
      <span className="gx-link"><Button><i className="icon icon-edit"/></Button></span>
      <Divider type="vertical"/>
      <span className="gx-link"><Button><i className="icon icon-trash"/></Button></span>
    </span>
    ),
  }];

  const data = [{
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
  }, {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
  }, {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  }, {
    key: '4',
    name: 'Disabled User',
    age: 99,
    address: 'Sidney No. 1 Lake Park',
  }];
  
  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };
class Paymentmode extends React.Component{
    state = {visible: false};
//     showModal = () => {
//       this.setState({
//         visible: true,
//       });
//     };
//     handleOk = (e) => {
//       console.log(e);
//       this.setState({
//         visible: false,
//       });
//     };
//     handleCancel = (e) => {
//       console.log(e);
//       this.setState({
//         visible: false,
//       });
//     };
    
//   handleSubmit = (e) => {
//     e.preventDefault();
//     this.props.form.validateFields((err, values) => {
//       if (!err) {
//         console.log('Received values of form: ', values);
//       }
//     });
//   };
//     componentDidMount() {
//         // To disabled submit button at the beginning.
//         this.props.form.validateFields();
//       }

render() {
   
  
    // const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched} = this.props.form;

    // Only show error after a field is touched.
    // const userNameError = isFieldTouched('userName') && getFieldError('userName');
    // const passwordError = isFieldTouched('password') && getFieldError('password');
   
return(
    <>
    <h1>art cpaymentmode</h1>
    <Row>
        <Col xl={6} lg={6} md={6} sm={12} xs={12}>
            <IconWithTextCard cardColor="blue" icon="table-data" title="12" subTitle="Category"/>
        </Col>
        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
        <Card title="user">
            <Table className="gx-table-responsive" rowSelection={rowSelection} columns={columns} dataSource={data} />
        </Card>
        </Col>
    </Row>
   
    </>
)

}


}

export default Paymentmode;