import React, { Component } from "react";
import { Card, Divider, Table, Button, Col, Row, Input, message, Form, Modal } from "antd";
import { connect } from "react-redux";
import DateWithoutTimeHelper from "./../helper/DateWithoutTimeHelper";
import IntlMessages from "util/IntlMessages"
import {
  getContcatus,
  setStatusToInitial,
  saveContactusData,
  deleteContactusData
} from "./../../appRedux/actions/Contactusactions";
import CircularProgress from "./../../components/CircularProgress/index";
import { userRolePermissionByUserId } from "./../../appRedux/actions/Auth";

import {FormattedMessage, injectIntl} from "react-intl";

let licenseId = '';

let IdentityId = '';

let langName = '';
const FormItem = Form.Item;
var searchAreaTerm = '';

class Contcatus extends Component {
  constructor() {
    super();
    this.state = {
      pagination: {
        pageSize: 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '40'],
      },
      addAreaModal: false,
      modalDeleteVisible: false,
      Id: '',
      area_name: '',
      editAreaFlag: '',
      delete_id: '',
      searchedArea: '',
    }
  }

  getAreaListById(pageNumber = '', sortBy = '-Id', perPage = '10', searchAreaTerm = '') {
    if (this.props.status == 'Initial') {
      this.props.getContcatus({'pageNumber': 1, sortBy: '-Id', 'perPage': perPage, 'searchAreaTerm': searchAreaTerm});
    }
    else {
      if (pageNumber === '') {
        pageNumber = 1;
      }
      if (perPage === '') {
        perPage = '10';
      }
      if (sortBy === '') {
        sortBy = '-Id';
      }
      this.props.getContcatus({'pageNumber': pageNumber, 'sortBy': sortBy, 'perPage': perPage, 'searchAreaTerm': searchAreaTerm});
    }
  }

  componentDidMount() {
       let userdata = localStorage.getItem('user_id');	
  langName = localStorage.getItem('selectedLanguage');	
  if (userdata !== '' && userdata !== null) {	
    let userData = JSON.parse(userdata);	
  console.log(userdata);
    if ((userData !== '' && userData !== null) && userData['IdentityId'] !== undefined && userData['id'] !== undefined ) {	
      licenseId = userData['id'];	
      IdentityId = userData['IdentityId'];	
    
    }	
  }	
  this.props.setStatusToInitial();	
  this.getAreaListById();	
  this.props.userRolePermissionByUserId(IdentityId);
  }

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    pager.current = pagination.current;
    pager.pageSize = pagination.pageSize;
    this.setState({
      pagination: pager,
    });

    var sortBy = '';
    if (sorter.order === 'ascend') {
      sortBy = '+' + sorter.field;
    } else if (sorter.order === 'descend') {
      sortBy = '-' + sorter.field;
    }
    this.getAreaListById(pagination.current, sortBy, pagination.pageSize, searchAreaTerm);
  };

  handleAreaSubmit = (e) => {
    e.preventDefault();
    var areaData = [];
    this.props.form.validateFieldsAndScroll(['areaName'], (err, values) => {
      if (!err) {
        if (this.state.editAreaFlag === 'edit') {
          areaData['id'] = this.state.Id;
          areaData['name'] = this.state.Name;
          areaData['email'] = this.state.Email;
          areaData['subject'] = this.state.Subject;
          areaData['message'] = this.state.message;
          areaData['id'] = IdentityId;

          var City_id = this.state.Id;
          var Name = this.state.Name;

          if (City_id !== '' && Name !== '' ) {
            this.setState({ addAreaModal: false });
            this.props.saveAreaData(areaData);
          }
          else {
            message.error(this.props.intl.formatMessage({id: 'global.TryAgain'}));
          }
        }
        else {

          areaData['id'] = IdentityId;
          areaData['id'] = this.state.Id;
          areaData['name'] = this.state.Name;
          areaData['email'] = this.state.Email;
          areaData['subject'] = this.state.Subject;
          areaData['message'] = this.state.message;
          var Name = this.state.Name;

          if (Name !== '' && IdentityId !== '') {
            this.setState({ addAreaModal: false });
            this.props.saveAreaData(areaData);
          }
          else {
            message.error(this.props.intl.formatMessage({id: 'global.TryAgain'}));
          }
        }
      }
    })
  };

  onAddArea = () => {
    this.setState({'Name': ''});

    this.setState({ addAreaModal: true });
    this.setState({ editAreaFlag: '' });
  };

  onEditArea = (a_id) => {
    var TaskData = this.props.getContcatusData.find((project) => {
      return a_id;
    })

    this.setState({ 'City_id': TaskData.Id });
    this.setState({ 'ArtCategoryName': TaskData.Name });

    this.setState({ addAreaModal: true });
    this.setState({ editAreaFlag: 'edit' });
  };

  closeAddArea = () => {
    this.setState({ addAreaModal: false });
  };

  onDeleteArea = (deleteId) => {
    this.setState({ modalDeleteVisible: true });
    this.setState({ delete_id: deleteId });
  };

  confirmDelete = () => {
    this.props.deleteAreaData({'deleteId': this.state.delete_id});
    this.setState({ modalDeleteVisible: false });
  }

  cancelDelete = (e) => {
    this.setState({ delete_id: '' });
    this.setState({ modalDeleteVisible: false });
  }

  handleChangeAreaSearch = (e) => {
    var tempSearchedArea = e.target.value;
    this.setState({ searchedArea: tempSearchedArea });
  }

  handleAreaSearch = (value) => {
    searchAreaTerm = value;
    var pagesize = this.state.pagination.pageSize;
    this.getAreaListById('', '', pagesize);
  }

  render() {
    var areasData= this.props.getContcatusData;
    var areaData = '';

    if (!areasData) {
      console.log("data not found");
      // Object is empty (Would return true in this example)
    }
    else {
      areaData = this.props.getContcatusData;
      const pagination = { ...this.state.pagination };
      var old_pagination_total = pagination.total;
      pagination.total = areasData.TotalCount;
      pagination.current = this.state.pagination.current ? this.state.pagination.current : 1;

      // var start_record = '';
      var end_record = '';
      if (pagination.current === 1) {
        // start_record = 1;
        end_record = pagination.pageSize;
      }
      else {
        // start_record = ((pagination.current -1) * pagination.pageSize) + 1;
        end_record = pagination.current * pagination.pageSize;
        if (end_record > pagination.total) {
          end_record = pagination.total;
        }
      }

      if (pagination.current !== '' && this.state.pagination.current === undefined) {
        this.setState({
          pagination
        });
      }
      else if (pagination.total !== '' && pagination.total !== old_pagination_total) {
        pagination.current = 1;
        this.setState({
          pagination
        });
      }
      else if ((pagination.total === '' || pagination.total === 0) && pagination.total !== old_pagination_total) {
        this.setState({
          pagination
        });
      }
    }

    const columns = [
      {
        title: 'id ',
        dataIndex: 'id',
        key: ' id',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'email',
        dataIndex: 'email',
        key: 'email',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'subject',
        dataIndex: 'subject',
        key: 'subject',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'message',
        dataIndex: 'message',
        key: 'message',
        render: text => <span className="">{text}</span>,
      },
      {
        title: <IntlMessages id="column.Action" />,
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <div>
            <FormattedMessage id="edit">
              {title =>
                <span className="gx-link">
                  <Button onClick={() => this.onEditArea(record.id)} value={record.id} className="arrow-btn gx-link">
               Edit
                  </Button>  
                </span>
              }
            </FormattedMessage>
            <Divider type="vertical" />
            <FormattedMessage id="delete">
              {title =>
                <span className="gx-link">
                   <Button onClick={() => this.onDeleteArea(record.id)} value={record.id} className="arrow-btn gx-link">
                    Delete
                  </Button> 
                </span>
              }
            </FormattedMessage>
          </div>
        ),
      }
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: record => ({
        FullName: record.FullName,
      }),
    };

    const { getFieldDecorator } = this.props.form;

    return (
      <Card className="custo_head_wrap" title={<IntlMessages id="Contact Us" />} extra={<div className="card-extra-form">

 <Button className="gx-mb-0" type="primary" style={{ float: "right" }} onClick={() => this.onAddArea()}>Add</Button>
      </div>}>

        <Table className="gx-table-responsive" rowSelection={rowSelection} columns={columns} dataSource={areaData} onChange={this.handleTableChange} pagination={this.state.pagination} size="middle" />
        <Modal
          title={this.state.editAreaFlag === 'edit' ? <IntlMessages id="Edit" /> : <IntlMessages id="Add" />}
          maskClosable={false}
          onCancel={this.closeAddArea}
          visible={this.state.addAreaModal}
          closable={true}
          okText={<IntlMessages id="save" />}
          cancelText={<IntlMessages id="cancel" />}
          onOk={this.handleAreaSubmit}
          destroyOnClose={true}
          className="cust-modal-width">
          <div className="gx-modal-box-row">
            <div className="gx-modal-box-form-item">
              <Form>
                <div className="gx-form-group">
                  <Row>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="name">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('name', {
                              initialValue: this.state.Name,
                              rules: [{
                                required: true,
                                message: "add name",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ Name: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="subject">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('subject', {
                              initialValue: this.state.Subject,
                              rules: [{
                                required: true,
                                message: "add subject",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ Subject: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="message">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('message', {
                              initialValue: this.state.message,
                              rules: [{
                                required: true,
                                message: "add message",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ message: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    
                  </Row>
                </div>
              </Form>
            </div>
          </div>
        </Modal>
        <Modal className=""
          title={<IntlMessages id="Contact Delete" />}
          visible={this.state.modalDeleteVisible}
          destroyOnClose={true}
          onCancel={() => this.cancelDelete()}
          onOk={() => this.confirmDelete()}
          okText={<IntlMessages id="Delete" />}
          cancelText={<IntlMessages id="Cancel" />}
        >
          <div className="gx-modal-box-row">
            <div className="gx-modal-box-form-item">
              <div className="mail-successbox">
                <h4 className="err-text"><IntlMessages id="Contact Delete" /></h4>
              </div>
            </div>
          </div>
        </Modal>
        {this.state.loader || this.props.loader ?
          <div className="gx-loader-view">
            <CircularProgress />
          </div> :
          null
        }
      </Card>
    );
  }
}

// Object of action creators
const mapDispatchToProps = {
  getContcatus,
  setStatusToInitial,
  saveContactusData,
  deleteContactusData,
  userRolePermissionByUserId
}

const viewAreaReportForm = Form.create()(Contcatus);

const mapStateToProps = state => {
  return {
    getContcatusData: state.ContactusReducers.get_Task_res,
    loader: state.ContactusReducers.loader,
    status: state.ContactusReducers.status,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(viewAreaReportForm));