import React, { Component } from "react";
import { Card, Divider, Table, Button, Col, Row, Input, message, Form, Modal } from "antd";
import { connect } from "react-redux";
import DateWithoutTimeHelper from "./../helper/DateWithoutTimeHelper";
import IntlMessages from "util/IntlMessages"
import {
  getProject,
  setStatusToInitial,
  saveProjectData,
  deleteProjectData
} from "./../../appRedux/actions/Projectactions";
import CircularProgress from "./../../components/CircularProgress/index";
import { userRolePermissionByUserId } from "./../../appRedux/actions/Auth";

import {FormattedMessage, injectIntl} from "react-intl";

let licenseId = '';
let IdentityId = '';

let langName = '';
const FormItem = Form.Item;
var searchAreaTerm = '';

class project extends Component {
  constructor() {
    super();
    this.state = {
      pagination: {
        pageSize: 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '40'],
      },
      addAreaModal: false,
      modalDeleteVisible: false,
      Id: '',
      area_name: '',
      editAreaFlag: '',
      delete_id: '',
      searchedArea: '',
    }
  }

  getAreaListById(pageNumber = '', sortBy = '-Id', perPage = '10', searchAreaTerm = '') {
    if (this.props.status == 'Initial') {
      this.props.getProject({'pageNumber': 1, sortBy: '-Id', 'perPage': perPage, 'searchAreaTerm': searchAreaTerm});
    }
    else {
      if (pageNumber === '') {
        pageNumber = 1;
      }
      if (perPage === '') {
        perPage = '10';
      }
      if (sortBy === '') {
        sortBy = '-Id';
      }
      this.props.getProject({'pageNumber': pageNumber, 'sortBy': sortBy, 'perPage': perPage, 'searchAreaTerm': searchAreaTerm});
    }
  }

  componentDidMount() {
       let userdata = localStorage.getItem('user_id');	
  langName = localStorage.getItem('selectedLanguage');	
  if (userdata !== '' && userdata !== null) {	
    let userData = JSON.parse(userdata);	
  
    if ((userData !== '' && userData !== null) && userData['IdentityId'] !== undefined && userData['id'] !== undefined ) {	
      licenseId = userData['id'];	
      IdentityId = userData['IdentityId'];	
    
    }	
  }	
  this.props.setStatusToInitial();	
  this.getAreaListById();	
  this.props.userRolePermissionByUserId(IdentityId);
  }

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    pager.current = pagination.current;
    pager.pageSize = pagination.pageSize;
    this.setState({
      pagination: pager,
    });

    var sortBy = '';
    if (sorter.order === 'ascend') {
      sortBy = '+' + sorter.field;
    } else if (sorter.order === 'descend') {
      sortBy = '-' + sorter.field;
    }
    this.getAreaListById(pagination.current, sortBy, pagination.pageSize, searchAreaTerm);
  };

  handleAreaSubmit = (e) => {
    e.preventDefault();
    var areaData = [];
    this.props.form.validateFieldsAndScroll(['areaName'], (err, values) => {
      if (!err) {
        if (this.state.editAreaFlag === 'edit') {
          areaData['Project_id'] = this.state.Project_id;
          areaData['Title'] = this.state.Title;
          areaData['Description'] = this.state.Description;
          areaData['Budget'] = this.state.Budget;
          areaData['Duration'] = this.state.Duration;
          areaData['User_id'] = this.state.User_Id;
          areaData['ArtCat_id'] = this.state.ArtCat_Id;
          areaData['SubCat_id'] = this.state.SubCat_Id;
          areaData['posted_on'] = this.state.posted_on;
          areaData['Is_close'] = this.state.Is_close;
          areaData['Project_id'] = IdentityId;

          var City_id = this.state.Project_id;
          var Title = this.state.Title;

          if (City_id !== '' && Title !== '' ) {
            this.setState({ addAreaModal: false });
            this.props.saveAreaData(areaData);
          }
          else {
            message.error(this.props.intl.formatMessage({id: 'global.TryAgain'}));
          }
        }
        else {
          areaData['Title'] = this.state.area_name;
          areaData['Project_id'] = IdentityId;
          areaData['Title'] = this.state.Title;
          areaData['Description'] = this.state.Description;
          areaData['Budget'] = this.state.Budget;
          areaData['Duration'] = this.state.Duration;
          areaData['User_id'] = this.state.User_Id;
          areaData['ArtCat_id'] = this.state.ArtCat_Id;
          areaData['SubCat_id'] = this.state.SubCat_Id;
          areaData['posted_on'] = this.state.posted_on;
          areaData['Is_close'] = this.state.Is_close;
          var Title = this.state.Title;

          if (Title !== '' ) {
            this.setState({ addAreaModal: false });
            this.props.saveAreaData(areaData);
          }
          else {
            message.error(this.props.intl.formatMessage({id: 'global.TryAgain'}));
          }
        }
      }
    })
  };

  onAddArea = () => {
    this.setState({'Title': ''});

    this.setState({ addAreaModal: true });
    this.setState({ editAreaFlag: '' });
  };

  onEditArea = (a_id) => {
    console.log('a_id',a_id);
    
    var TaskData = this.props.getTaskData.find((project) => {
     
      return a_id;
    })
  
    this.setState({ 'Project_id': TaskData.Id });
    this.setState({ 'Title': TaskData.Title });
    this.setState({'Description': TaskData.Description});
    this.setState({'Budget': TaskData.Budget});
    this.setState({'Duration': TaskData.Duration});
    this.setState({'User_id': TaskData.User_id});
    this.setState({'ArtCat_id': TaskData.ArtCat_id});
    this.setState({'SubCat_id': TaskData.SubCat_id});
    this.setState({'posted_on': TaskData.posted_on});
    this.setState({'Is_close': TaskData.Is_close});
   

    this.setState({ addAreaModal: true });
    this.setState({ editAreaFlag: 'edit' });
  };

  closeAddArea = () => {
    this.setState({ addAreaModal: false });
  };

  onDeleteArea = (deleteId) => {
 
    this.setState({ modalDeleteVisible: true });
    this.setState({ delete_id: deleteId });
  };

  confirmDelete = () => {
    this.props.deleteAreaData({'deleteId': this.state.delete_id});
    this.setState({ modalDeleteVisible: false });
  }

  cancelDelete = (e) => {
    this.setState({ delete_id: '' });
    this.setState({ modalDeleteVisible: false });
  }

  handleChangeAreaSearch = (e) => {
    var tempSearchedArea = e.target.value;
    this.setState({ searchedArea: tempSearchedArea });
  }

  handleAreaSearch = (value) => {
    searchAreaTerm = value;
    var pagesize = this.state.pagination.pageSize;
    this.getAreaListById('', '', pagesize);
  }

  render() {
    var areasData= this.props.getTaskData;
    var areaData = '';

    if (!areasData) {
      console.log("data not found");
      // Object is empty (Would return true in this example)
    }
    else {

      areaData = this.props.getTaskData;

      const pagination = { ...this.state.pagination };
      var old_pagination_total = pagination.total;

      pagination.total = areasData.TotalCount;
      pagination.current = this.state.pagination.current ? this.state.pagination.current : 1;

      // var start_record = '';
      var end_record = '';
      if (pagination.current === 1) {
        // start_record = 1;
        end_record = pagination.pageSize;
      }
      else {
        // start_record = ((pagination.current -1) * pagination.pageSize) + 1;
        end_record = pagination.current * pagination.pageSize;
        if (end_record > pagination.total) {
          end_record = pagination.total;
        }
      }

      if (pagination.current !== '' && this.state.pagination.current === undefined) {
        this.setState({
          pagination
        });
      }
      else if (pagination.total !== '' && pagination.total !== old_pagination_total) {
        pagination.current = 1;
        this.setState({
          pagination
        });
      }
      else if ((pagination.total === '' || pagination.total === 0) && pagination.total !== old_pagination_total) {
        this.setState({
          pagination
        });
      }
    }

    const columns = [
      {
        title: 'Project_id',
        dataIndex: 'Project_id',
        key: 'Project_id',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Title',
        dataIndex: 'Title',
        key: 'Title',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Description',
        dataIndex: 'Description',
        key: 'Description',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Budget',
        dataIndex: 'Budget',
        key: 'Budget',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Duration',
        dataIndex: 'Duration',
        key: 'Duration',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'User_id',
        dataIndex: 'User_id',
        key: 'User_id',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'ArtCat_id',
        dataIndex: 'ArtCat_id',
        key: 'ArtCat_id',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'SubCat_id',
        dataIndex: 'SubCat_id',
        key: 'SubCat_id',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'posted_on',
        dataIndex: 'posted_on',
        key: 'posted_on',
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Is_close',
        dataIndex: 'Is_close',
        key: 'Is_close',
        render: text => <span className="">{text}</span>,
      },
      {
        title: <IntlMessages id="Action" />,
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <div>
            <FormattedMessage id="Edit">
              {title =>
                <span className="gx-link">
                  <Button onClick={() => this.onEditArea(record.Project_id)} value={record.Project_id} className="arrow-btn gx-link">
               Edit
                  </Button>  
                </span>
              }
            </FormattedMessage>
            <Divider type="vertical" />
            <FormattedMessage id="Delete">
              {title =>
                <span className="gx-link">
                   <Button onClick={() => this.onDeleteArea(record.Project_id)} value={record.Project_id} className="arrow-btn gx-link">
                    Delete
                  </Button> 
                </span>
              }
            </FormattedMessage>
          </div>
        ),
      }
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: record => ({
        FullName: record.FullName,
      }),
    };

    const { getFieldDecorator } = this.props.form;

    return (

      <Card className="custo_head_wrap" title={<IntlMessages id="Project" />} extra={<div className="card-extra-form">

 <Button className="gx-mb-0" type="primary" style={{ float: "right" }} onClick={() => this.onAddArea()}>Add</Button>
      </div>}>

        <Table className="gx-table-responsive" rowSelection={rowSelection} columns={columns} dataSource={areaData} onChange={this.handleTableChange} pagination={this.state.pagination} size="middle" />
        <Modal
          title={this.state.editAreaFlag === 'edit' ? <IntlMessages id="project Edit" /> : <IntlMessages id="project Add" />}
          maskClosable={false}
          onCancel={this.closeAddArea}
          visible={this.state.addAreaModal}
          closable={true}
          okText={<IntlMessages id="save" />}
          cancelText={<IntlMessages id="cancel" />}
          onOk={this.handleAreaSubmit}
          destroyOnClose={true}
          className="cust-modal-width">
          <div className="gx-modal-box-row">
            <div className="gx-modal-box-form-item">
              <Form>
                <div className="gx-form-group">
                  <Row>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="Project title">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('Title', {
                              initialValue: this.state.Title,
                              rules: [{
                                required: true,
                                message: "add Title",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ Title: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="Description">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('Description', {
                              initialValue: this.state.Description,
                              rules: [{
                                required: true,
                                message: "add Description",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ Description: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="Budget">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('Budget', {
                              initialValue: this.state.Budget,
                              rules: [{
                                required: true,
                                message: "add Budget",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ Budget: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="Duration">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('Duration', {
                              initialValue: this.state.Duration,
                              rules: [{
                                required: true,
                                message: "add Duration",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ Duration: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                    <Col lg={24} xs={24}>
                      <FormattedMessage id="posted_on">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('posted_on', {
                              initialValue: this.state.posted_on,
                              rules: [{
                                required: true,
                                message: "add posted_on",
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ posted_on: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                  </Row>
                </div>
              </Form>
            </div>
          </div>
        </Modal>


        <Modal className=""
          title={<IntlMessages id="ProjectDelete" />}
          visible={this.state.modalDeleteVisible}
          destroyOnClose={true}
          onCancel={() => this.cancelDelete()}
          onOk={() => this.confirmDelete()}
          okText={<IntlMessages id="Delete" />}
          cancelText={<IntlMessages id="Cancel" />}
        >
          <div className="gx-modal-box-row">
            <div className="gx-modal-box-form-item">
              <div className="mail-successbox">
                <h4 className="err-text"><IntlMessages id="Project Confirm Delete" /></h4>
               
              </div>
            </div>
          </div>
        </Modal>
        {this.state.loader || this.props.loader ?
          <div className="gx-loader-view">
            <CircularProgress />
          </div> :
          null
        }
      </Card>
    );
  }
}

// Object of action creators
const mapDispatchToProps = {
  getProject,
  setStatusToInitial,
  saveProjectData,
  deleteProjectData,
  userRolePermissionByUserId
}

const viewAreaReportForm = Form.create()(project);

const mapStateToProps = state => {
  return {
    getTaskData: state.ProjectReducers.get_Task_res,
    loader: state.ProjectReducers.loader,
    status: state.ProjectReducers.status,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(viewAreaReportForm));