import React from "react"
import { Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const Table = ({match}) => (
    <Switch>
    {/* <Redirect exact from={`${match.url}/Artcat`} to={`${match.url}/Artcat`}/> */}
    <Route path={`${match.url}/Task`} component={asyncComponent(() => import('./Task'))}/>

    <Route path={`${match.url}/Artcat`} component={asyncComponent(() => import('./Artcat'))}/>
    <Route path={`${match.url}/Artsubcat`} component={asyncComponent(() => import('./Artsubcat'))}/>
    <Route path={`${match.url}/Bidding`} component={asyncComponent(() => import('./Bidding'))}/>
    <Route path={`${match.url}/Contactus`} component={asyncComponent(() => import('./Contactus'))}/>
    <Route path={`${match.url}/Expereincelevel`} component={asyncComponent(() => import('./Expereincelevel'))}/>
    <Route path={`${match.url}/Login`} component={asyncComponent(() => import('./Login'))}/>
    <Route path={`${match.url}/Payment`} component={asyncComponent(() => import('./Payment'))}/>
    <Route path={`${match.url}/City`} component={asyncComponent(() => import('./City'))}/>
    <Route path={`${match.url}/Feedback`} component={asyncComponent(() => import('./Feedback'))}/>
    <Route path={`${match.url}/Paymentmode`} component={asyncComponent(() => import('./Paymentmode'))}/>
    <Route path={`${match.url}/Project`} component={asyncComponent(() => import('./Project'))}/>
    <Route path={`${match.url}/Registration`} component={asyncComponent(() => import('./Registration'))}/>
    <Route path={`${match.url}/Subartcat`} component={asyncComponent(() => import('./Subartcat'))}/>
    <Route path={`${match.url}/Artsubcat`} component={asyncComponent(() => import('./Artsubcat'))}/>
    <Route path={`${match.url}/Userartcat`} component={asyncComponent(() => import('./Userartcat'))}/>
    <Route path={`${match.url}/Usertype`} component={asyncComponent(() => import('./Usertype'))}/>
   
  </Switch>
)    

export default Table;