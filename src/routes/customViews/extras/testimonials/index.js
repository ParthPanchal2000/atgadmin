import React from "react";
import {Col, Row} from "antd";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


import Classic from "./Classic/index";

import { testimonialsData} from "./testimonialsData";
import IntlMessages from "util/IntlMessages";
import CardBox from "components/CardBox/index";
import ContainerHeader from "components/ContainerHeader/index";

const Testimonials =(props)=> {

 

    const options1 = {
      dots: true,
      infinite: true,
      speed: 500,
      marginLeft: 10,
      marginRight: 10,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]

    };

    

    return (
      <div className="gx-main-content">
        <ContainerHeader match={props.match}
                         title={<IntlMessages id="extraElements.testimonials"/>}
                         description="In promotion and of advertising, a testimonial or show consists of a person's written or spoken statement extolling the virtue of a product. "
        />

        <Row>
         
          <Col span={24}>
            <CardBox styleName="gx-text-center" heading={<IntlMessages id="testimonials.classic"/>}>
              <Slider  {...options1}>
                <Classic testimonial={testimonialsData[0]}/>
                <Classic testimonial={testimonialsData[1]}/>
                <Classic testimonial={testimonialsData[2]}/>
                <Classic testimonial={testimonialsData[3]}/>
                <Classic testimonial={testimonialsData[4]}/>
                <Classic testimonial={testimonialsData[5]}/>
                <Classic testimonial={testimonialsData[6]}/>
              </Slider>
            </CardBox>
          </Col>
         
        </Row>
      </div>
    )
  };

export default Testimonials;
