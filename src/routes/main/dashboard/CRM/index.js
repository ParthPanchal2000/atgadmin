import React from "react";
import {Col, Row} from "antd";
import {Link} from "react-router-dom";
import IconWithTextCard from "components/Metrics/IconWithTextCard";
import SiteVisit from "components/dashboard/CRM/SiteVisit";
import WelComeCard from "components/dashboard/CRM/WelComeCard";
import SiteAudience from "components/dashboard/CRM/SiteAudience";
import Auxiliary from "util/Auxiliary";


import {trafficData} from "./data";

const CRM = () => {
  return (
    <Auxiliary>
      <Row>
      {/* <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
      <Link to="/components/table/basic"> <IconWithTextCard icon="dimond" iconColor="geekblue" title="2,380" subTitle=""/></Link>
        </Col> */}
        <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
        <Link to="/Table/Project"> <IconWithTextCard icon="folder-o" iconColor="primary" title="2,380" subTitle="Project"/></Link>
        </Col>
        <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
        <Link to="/Table/Usertype"> <IconWithTextCard icon="avatar" iconColor="geekblue" title="2,380" subTitle="Artist"/></Link>
        </Col>
        <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
        <Link to="/Table/Feedback"> <IconWithTextCard icon="pricing-table" iconColor="geekblue" title="2,380" subTitle="Feedback"/></Link>
        </Col>
        {/* <Col span={24}>
          <div className="gx-card">
            <div className="gx-card-body">
              <Row>
                <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                  <WelComeCard/>
                </Col>

                <Col xl={6} lg={12} md={12} sm={12} xs={24} className="gx-audi-col">
                  <SiteAudience/>
                </Col>

                <Col xl={12} lg={24} md={24} sm={24} xs={24} className="gx-visit-col">
                  <SiteVisit/>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
        */}
        
        

        
      </Row>
    </Auxiliary>
  );
};

export default CRM;
