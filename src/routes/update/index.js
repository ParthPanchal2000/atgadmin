import React from "react"
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const Update = ({match}) => (
    <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/login`}/>
    <Route path={`${match.url}/ProfileUpdate`} component={asyncComponent(() => import('./ProfileUpdate'))}/>
    <Route path={`${match.url}/ProjectUpdate`} component={asyncComponent(() => import('./ProjectUpdate'))}/>
  </Switch>
)    

export default Update;