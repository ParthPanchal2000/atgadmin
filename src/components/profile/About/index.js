import React from "react";
import {Col, Row, Tabs} from "antd";
import Widget from "components/Widget";
import {aboutList} from '../../../routes/socialApps/Profile/data'
import AboutItem from "./AboutItem";


const TabPane = Tabs.TabPane;

const About = () => {

    return (
      <Widget title="About" styleName="gx-card-tabs gx-card-tabs-right gx-card-profile">
         
        <Tabs defaultActiveKey="1">
          <TabPane tab="Overview" key="1">
        
            <div className="gx-mb-2">
             <span className="gx-link gx-profile-setting">
              <i className="icon icon-edit gx-fs-lg gx-mr-2 gx-mr-sm-3 gx-d-inline-flex gx-vertical-align-middle"/>
              <span className="gx-d-inline-flex gx-vertical-align-middle gx-ml-1 gx-ml-sm-0">Edit</span>
             </span>
              <Row>
                {aboutList.map((about, index) =>
                  <Col key={index} xl={8} lg={12} md={12} sm={12} xs={24}>
                    <AboutItem data={about}/>
                  </Col>
                )}
              </Row>
            </div>
          </TabPane>
        
        </Tabs>
      </Widget>
    );
}


export default About;
