import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import Settings from "./Settings";
import Auth from "./Auth";
import Notes from "./Notes";
import Contact from "./Contact";
import Common from "./Common";
import AreaReducers from "./AreaReducers";
import TaskReducer from "./TaskReducer";
import ArtcatReducer from './ArtcatReducers';
import ArtsubcatReducer from './ArtsubcatReducer';
import LoginReducers from './LoginReducers';
import ContactusReducers from './ContactusReducers';
import BidReducers from './BidReducers';
import ExpereincelevelReducers from './ExpereincelevelReducers';
import FeedbackReducers from './FeedbackReducers';
import ProjectReducers from './ProjectReducers';
import UsertypeReducers from './UsertypeReducers';
import SubartcatReducers from './SubartcatReducers';
import UserartcatReducers from './UserartcatReducers';
import RegistrationReducers from './RegistrationReducers';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  settings: Settings,
  auth: Auth,
  notes: Notes,
  contact: Contact,
  common: Common,
  TaskReducer:TaskReducer,
  AreaReducers:AreaReducers,
  ArtcatReducer:ArtcatReducer,
  ArtsubcatReducer:ArtsubcatReducer,
  LoginReducers:LoginReducers,
  ContactusReducers:ContactusReducers,
  BidReducers:BidReducers,
  ExpereincelevelReducers:ExpereincelevelReducers,
  FeedbackReducers:FeedbackReducers,
  ProjectReducers:ProjectReducers,
  UsertypeReducers:UsertypeReducers,
  SubartcatReducers:SubartcatReducers,
  UserartcatReducers:UserartcatReducers,
  RegistrationReducers:RegistrationReducers,
});

export default createRootReducer
