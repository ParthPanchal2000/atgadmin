import {
    GET_BID,
    GET_BID_SUCCESS_DATA,
    SAVE_CONTACTUS_DATA,
    DELETE_BID_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getBid = (area) => {
    return {
      type: GET_BID,
      payload: area
    };
  };
 

  export const getContactusSuccess = (data) => {
    return {
      type: GET_BID_SUCCESS_DATA,
      payload: data
    };
  };

  export const saveBidData = (Data) => {
    return {
      type: SAVE_CONTACTUS_DATA,
      payload: Data
    };
  };

  export const deleteBidData = (Data) => {
    return {
      type: DELETE_BID_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  