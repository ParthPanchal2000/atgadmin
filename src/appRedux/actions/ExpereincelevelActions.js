import {
    GET_Expereincelevel,
    GET_Expereincelevel_SUCCESS_DATA,
    SAVE_Expereincelevel_DATA,
    DELETE_Expereincelevel_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getExpereincelevel = (area) => {
    return {
      type: GET_Expereincelevel,
      payload: area
    };
  };

  export const getExpereincelevelSuccess = (data) => {
    return {
      type: GET_Expereincelevel_SUCCESS_DATA,
      payload: data
    };
  };

  export const saveExpereincelevelData = (Data) => {
    return {
      type: SAVE_Expereincelevel_DATA,
      payload: Data
    };
  };

  export const deleteExpereincelevelData = (Data) => {
    return {
      type: DELETE_Expereincelevel_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };