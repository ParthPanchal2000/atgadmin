import {
    GET_USERTYPE,
    GET_USERTYPE_SUCCESS_DATA,
    SAVE_USERTYPE_DATA,
    DELETE_USERTYPE_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
  
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getUsertype = (area) => {
    return {
      type: GET_USERTYPE,
      payload: area
    };
  };
  
  
  export const getUsertypeSuccess = (data) => {
    return {
      type: GET_USERTYPE_SUCCESS_DATA,
      payload: data
    };
  };
  
  export const saveUsertypeData = (Data) => {
    return {
      type: SAVE_USERTYPE_DATA,
      payload: Data
    };
  };
  
  export const deleteUsertypeData = (Data) => {
    return {
      type: DELETE_USERTYPE_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  