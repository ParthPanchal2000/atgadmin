import {
    GET_CONTACTUS,
    GET_CONTACTUS_SUCCESS_DATA,
    SAVE_CONTACTUS_DATA,
    DELETE_CONTACTUS_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getContcatus = (area) => {
    return {
      type: GET_CONTACTUS,
      payload: area
    };
  };
 

  export const getContactusSuccess = (data) => {
    return {
      type: GET_CONTACTUS_SUCCESS_DATA,
      payload: data
    };
  };

  export const saveContactusData = (Data) => {
    return {
      type: SAVE_CONTACTUS_DATA,
      payload: Data
    };
  };

  export const deleteContactusData = (Data) => {
    return {
      type: DELETE_CONTACTUS_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  