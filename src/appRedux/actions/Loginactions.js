import {
  GET_LOGIN,
  GET_LOGIN_SUCCESS_DATA,
  SAVE_LOGIN_DATA,
  DELETE_LOGIN_DATA,
  GET_STATUS_INITIAL,
  USER_ROLE_PERMISSION,

  SHOW_MESSAGE
} from "constants/ActionTypes";

export const getLogin = (area) => {
  return {
    type: GET_LOGIN,
    payload: area
  };
};


export const getLoginSuccess = (data) => {
  return {
    type: GET_LOGIN_SUCCESS_DATA,
    payload: data
  };
};

export const saveLoginData = (Data) => {
  return {
    type: SAVE_LOGIN_DATA,
    payload: Data
  };
};

export const deleteLoginData = (Data) => {
  return {
    type: DELETE_LOGIN_DATA,
    payload: Data
  };
};

export const setStatusToInitial = () => {
  return {
    type: GET_STATUS_INITIAL,
  };
};

export const showErrorMessage = (message) => {
  return {
    type: SHOW_MESSAGE,
    payload: message
  };
};
export const userRolePermissionByUserId = (Data) => {
  return {
    type: USER_ROLE_PERMISSION,
    payload: Data
  };
};
