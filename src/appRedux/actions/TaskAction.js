import {
    GET_TASK,
    GET_TASK_SUCCESS_DATA,
    SAVE_TASK_DATA,
    DELETE_TASK_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getTask = (area) => {
    return {
      type: GET_TASK,
      payload: area
    };
  };

  export const getAreasSuccess = (data) => {
    return {
      type: GET_TASK_SUCCESS_DATA,
      payload: data
    };
  };

  export const saveTaskData = (Data) => {
    return {
      type: SAVE_TASK_DATA,
      payload: Data
    };
  };

  export const deleteTaskData = (Data) => {
    return {
      type: DELETE_TASK_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };