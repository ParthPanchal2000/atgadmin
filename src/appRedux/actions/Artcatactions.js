import {
    GET_ART_CAT,
    GET_ART_CAT_SUCCESS_DATA,
    SAVE_ART_CAT_DATA,
    DELETE_ART_CAT_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
  
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getArtCat = (area) => {
    return {
      type: GET_ART_CAT,
      payload: area
    };
  };
 

  export const getArtCatSuccess = (data) => {
    return {
      type: GET_ART_CAT_SUCCESS_DATA,
      payload: data
    };
  };

  export const saveArtCatData = (Data) => {
    return {
      type: SAVE_ART_CAT_DATA,
      payload: Data
    };
  };

  export const deleteArtCatData = (Data) => {
    return {
      type: DELETE_ART_CAT_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  