

import {
    GET_FEEDBACK,
    GET_FEEDBACK_SUCCESS_DATA,
    SAVE_FEEDBACK_DATA,
    DELETE_FEEDBACK_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getFeedback = (area) => {
    return {
      type: GET_FEEDBACK,
      payload: area
    };
  };
 

  export const getFeedbackSuccess = (data) => {
    return {
      type: GET_FEEDBACK_SUCCESS_DATA,
      payload: data
    };
  };

  export const saveFeedbackData = (Data) => {
    return {
      type: SAVE_FEEDBACK_DATA,
      payload: Data
    };
  };

  export const deleteFeedbackData = (Data) => {
    return {
      type: DELETE_FEEDBACK_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  

