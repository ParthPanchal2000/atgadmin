import {
    GET_REGISTRATION,
    GET_REGISTRATION_SUCCESS_DATA,
    SAVE_REGISTRATION_DATA,
    DELETE_REGISTRATION_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
  
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getRegistration = (area) => {
    return {
      type: GET_REGISTRATION,
      payload: area
    };
  };
  
  
  export const getRegistrationSuccess = (data) => {
    return {
      type: GET_REGISTRATION_SUCCESS_DATA,
      payload: data
    };
  };
  
  export const saveRegistrationData = (Data) => {
    return {
      type: SAVE_REGISTRATION_DATA,
      payload: Data
    };
  };
  
  export const deleteRegistrationData = (Data) => {
    return {
      type: DELETE_REGISTRATION_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  