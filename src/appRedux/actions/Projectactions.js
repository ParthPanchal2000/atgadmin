import {
    GET_PROJECT,
    GET_PROJECT_SUCCESS_DATA,
    SAVE_PROJECT_DATA,
    DELETE_PROJECT_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
  
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getProject = (area) => {
    return {
      type: GET_PROJECT,
      payload: area
    };
  };
  
  
  export const getProjectSuccess = (data) => {
    return {
      type: GET_PROJECT_SUCCESS_DATA,
      payload: data
    };
  };
  
  export const saveProjectData = (Data) => {
    return {
      type: SAVE_PROJECT_DATA,
      payload: Data
    };
  };
  
  export const deleteProjectData = (Data) => {
    return {
      type: DELETE_PROJECT_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  