import {
    GET_SUBARTCAT,
    GET_SUBARTCAT_SUCCESS_DATA,
    SAVE_SUBARTCAT_DATA,
    DELETE_SUBARTCAT_DATA,
    GET_STATUS_INITIAL,
    USER_ROLE_PERMISSION,
  
    SHOW_MESSAGE
  } from "constants/ActionTypes";
  
  export const getSubartcat = (area) => {
    return {
      type: GET_SUBARTCAT,
      payload: area
    };
  };
  
  
  export const getSubartcatSuccess = (data) => {
    return {
      type: GET_SUBARTCAT_SUCCESS_DATA,
      payload: data
    };
  };
  
  export const saveSubartcatData = (Data) => {
    return {
      type: SAVE_SUBARTCAT_DATA,
      payload: Data
    };
  };
  
  export const deleteSubartcatData = (Data) => {
    return {
      type: DELETE_SUBARTCAT_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = (message) => {
    return {
      type: SHOW_MESSAGE,
      payload: message
    };
  };
  export const userRolePermissionByUserId = (Data) => {
    return {
      type: USER_ROLE_PERMISSION,
      payload: Data
    };
  };
  