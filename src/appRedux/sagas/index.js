import {all} from "redux-saga/effects";
import authSagas from "./Auth";
import notesSagas from "./Notes";
import AreaSagas from "./AreaSagas";
import TaskSaga from "./TaskSaga";
import ArtcatSagas from "./ArtcatSagas";
import ArtsubcatSagas from "./ArtsubcatSagas";
import LoginSagas from './LoginSagas';
import ContactusSagas from './ContactusSagas';
import BidSagas from './BidSagas';
import ExpereincelevelSagas from './ExpereincelevelSagas';
import FeedbackSagas from './FeedbackSagas';
import UsertypeSagas from './UsertypeSagas';
import SubartcatSagas from './SubartcatSagas';
import UserartcatSagas from './UserartcatSagas';
import ProjectSagas from './ProjectSagas';
import RegistartionSagas from './RegistrationSagas';

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    notesSagas(),
    AreaSagas(),
    TaskSaga(),
    ArtcatSagas(),
    ArtsubcatSagas(),
    LoginSagas(),
    ContactusSagas(),
    BidSagas(),
    ExpereincelevelSagas(),
    FeedbackSagas(),
    UsertypeSagas(),
    SubartcatSagas(),
    UserartcatSagas(),
    ProjectSagas(),
    RegistartionSagas()
  ]);
}
