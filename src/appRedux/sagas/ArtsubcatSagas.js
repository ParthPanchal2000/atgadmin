import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import axios from 'axios';
import {baseURL} from './../../util/config';
import {
    GET_ART_SUB_CAT,
    SAVE_ART_SUB_CAT_DATA,
    DELETE_ART_SUB_CAT_DATA
} from "./../../../src/constants/ActionTypes";
import {showErrorMessage, getAreasSuccess} from "../actions/Artsubcatactions";
import { message } from "antd";

export const token = (state) => state.token;
let licenseId = '';
let langName = '';

const headers = {
    'Content-Type': 'application/json'
}

/*area api call section start*/
const getTaskByLicenseId = async(payloadData) => await axios.get(baseURL +'ah-backend/api/amin/data2/' +'ArtsubcatList?PageNumber='+payloadData.pageNumber+'&PerPage='+payloadData.perPage+'&Sort='+payloadData.sortBy, {headers: headers})
    .then(getTaskRes => getTaskRes.data)
    .catch(error => error);

const saveAreaAPIcall = async(payloadData) => await axios.post(baseURL + 'UpsertArea?lang='+langName,payloadData,{headers: headers})
    .then(getSaveRes => getSaveRes.data)
    .catch(error => error);

const deleteAreaAPIcall = async(payloadData) => await axios.delete(baseURL + 'DeleteArea?lang='+langName+'&LicenseId='+licenseId+'&Id='+payloadData.deleteId, {headers: headers})
    .then(getDeleteRes => getDeleteRes.data)
    .catch(error => error);

/*area api call section end*/

//area api call function start
function * getAreaById({payload}) {
    if (payload === '' || payload === undefined) {
        payload = {'pageNumber': '', sortBy : '', 'perPage' : '', 'searchAreaTerm': ''};
    }

    try {
        const getTaskRes = yield call(getTaskByLicenseId,payload);
        if (getTaskRes.status) {
            console.log(getTaskRes.data);
            yield put(getAreasSuccess(getTaskRes.data));
        } else {
            yield put(showErrorMessage(getTaskRes.message));
        }
    } catch (error) {
        yield put(showErrorMessage(error));
    }
}

function * saveArea({payload}) {
    try { 
        var areaObject = Object.assign({}, payload);
        langName = localStorage.getItem('selectedLanguage');
        const getSaveResult = yield call(saveAreaAPIcall, areaObject);
        if (getSaveResult.status) {
            message.success(getSaveResult.message);
            var payloadData = {'pageNumber': 1, sortBy : '-Id', 'perPage' : 10, 'searchAreaTerm': ''};
            const getTaskRes = yield call(getTaskByLicenseId, payloadData);
            if (getTaskRes.status) {
                yield put(getAreasSuccess(getTaskRes.data));
            } else {
                yield put(showErrorMessage(getTaskRes.message));
            }
        } else {
            yield put(showErrorMessage(getSaveResult.message));
            message.error(getSaveResult.message);
        }
    } catch (error) {
        yield put(showErrorMessage(error));
    }
}

function * deleteArea({payload}) {
    let userdata = localStorage.getItem('city_id');
    langName = localStorage.getItem('selectedLanguage');
    if (userdata !== '' && userdata !== null)
    {
        let userData = JSON.parse(userdata);
        if((userData !== '' && userData!== null) && userData['id'] !== undefined)
        {
            licenseId = userData['City_id'];
        }    
    }

    try {
        const getDeleteResult = yield call(deleteAreaAPIcall, payload);
        if (getDeleteResult.status) {
            message.success(getDeleteResult.message);
            var payloadData = {'pageNumber': 1, sortBy : '-Id', 'perPage' : 10, 'searchAreaTerm': ''};
            const getTaskRes = yield call(getTaskByLicenseId, payloadData);
            if (getTaskRes.status) {
                yield put(getAreasSuccess(getTaskRes.data));
            } else {
                yield put(showErrorMessage(getTaskRes.message));
            }
        } else { 
            yield put(showErrorMessage(getDeleteResult.message));
            message.error(getDeleteResult.message);
        }
    } catch (error) {
        yield put(showErrorMessage(error));
    }
}
//area api call function end

//take every function call
export function * getTask() {
    yield takeEvery(GET_ART_SUB_CAT, getAreaById);
}
export function * addTask() {
    yield takeEvery(SAVE_ART_SUB_CAT_DATA, saveArea);
}
export function * removeTask() {
    yield takeEvery(DELETE_ART_SUB_CAT_DATA, deleteArea);
}
export default function * rootSaga() {
    yield all([
        fork(getTask),
        fork(addTask),
        fork(removeTask)
    ]);
}
