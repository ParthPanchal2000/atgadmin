import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import axios from 'axios';
import {baseURL} from '../../util/config';
import {
    GET_SUBARTCAT,
     SAVE_SUBARTCAT_DATA,
     DELETE_SUBARTCAT_DATA

} from "../../constants/ActionTypes";
import {showErrorMessage, getSubartcatSuccess} from "../actions/Subartcatactions";
import { message } from "antd";

export const token = (state) => state.token;
let licenseId = '';
let langName = '';

const headers = {
    'Content-Type': 'application/json'
}

/*area api call section start*/
const getTaskByLicenseId = async(payloadData) => await axios.get(baseURL +'ah-backend/api/amin/data11/' +'ArtcatList?PageNumber='+payloadData.pageNumber+'&PerPage='+payloadData.perPage+'&Sort='+payloadData.sortBy, {headers: headers})
    .then(getTaskRes => getTaskRes.data)
    .catch(error => error);

const saveAreaAPIcall = async(payloadData) => await axios.post(baseURL + 'UpsertArea?lang='+langName,payloadData,{headers: headers})
    .then(getSaveRes => getSaveRes.data)
    .catch(error => error);

const deleteAreaAPIcall = async(payloadData) => await axios.delete(baseURL + 'DeleteArea?lang='+langName+'&LicenseId='+licenseId+'&Id='+payloadData.deleteId, {headers: headers})
    .then(getDeleteRes => getDeleteRes.data)
    .catch(error => error);

/*area api call section end*/

//area api call function start
function * getArtCatById({payload}) {
    if (payload === '' || payload === undefined) {
        payload = {'pageNumber': '', sortBy : '', 'perPage' : '', 'searchAreaTerm': ''};
    }

    try {
        const getTaskRes = yield call(getTaskByLicenseId,payload);
        if (getTaskRes.status) {
            console.log(getTaskRes.data);
            yield put(getSubartcatSuccess(getTaskRes.data));
        } else {
            yield put(showErrorMessage(getTaskRes.message));
        }
    } catch (error) {
        yield put(showErrorMessage(error));
    }
}

function * saveArea({payload}) {
    try { 
        var areaObject = Object.assign({}, payload);
        langName = localStorage.getItem('selectedLanguage');
        const getSaveResult = yield call(saveAreaAPIcall, areaObject);
        if (getSaveResult.status) {
            message.success(getSaveResult.message);
            var payloadData = {'pageNumber': 1, sortBy : '-Id', 'perPage' : 10, 'searchAreaTerm': ''};
            const getTaskRes = yield call(getTaskByLicenseId, payloadData);
            if (getTaskRes.status) {
                yield put(getSubartcatSuccess(getTaskRes.data));
            } else {
                yield put(showErrorMessage(getTaskRes.message));
            }
        } else {
            yield put(showErrorMessage(getSaveResult.message));
            message.error(getSaveResult.message);
        }
    } catch (error) {
        yield put(showErrorMessage(error));
    }
}

function * deleteArea({payload}) {
    let userdata = localStorage.getItem('login_id');
    langName = localStorage.getItem('selectedLanguage');
    if (userdata !== '' && userdata !== null)
    {
        let userData = JSON.parse(userdata);
        if((userData !== '' && userData!== null) && userData['id'] !== undefined)
        {
            licenseId = userData['Login_id'];
        }    
    }

    try {
        const getDeleteResult = yield call(deleteAreaAPIcall, payload);
        if (getDeleteResult.status) {
            message.success(getDeleteResult.message);
            var payloadData = {'pageNumber': 1, sortBy : '-Id', 'perPage' : 10, 'searchAreaTerm': ''};
            const getTaskRes = yield call(getTaskByLicenseId, payloadData);
            if (getTaskRes.status) {
                yield put(getSubartcatSuccess(getTaskRes.data));
            } else {
                yield put(showErrorMessage(getTaskRes.message));
            }
        } else { 
            yield put(showErrorMessage(getDeleteResult.message));
            message.error(getDeleteResult.message);
        }
    } catch (error) {
        yield put(showErrorMessage(error));
    }
}
//area api call function end

//take every function call
export function * getLogin() {
    yield takeEvery(GET_SUBARTCAT, getArtCatById);
}
export function * addLogin() {
    yield takeEvery( SAVE_SUBARTCAT_DATA, saveArea);
}
export function * removeLogin() {
    yield takeEvery( DELETE_SUBARTCAT_DATA, deleteArea);
}
export default function * rootSaga() {
    yield all([
        fork(getLogin),
        fork(addLogin),
        fork(removeLogin)
    ]);
}
